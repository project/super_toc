CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
A powerful yet user friendly module that automatically creates a context
specific index or table of contents (TOC) for long pages (for any node types).

Built from the ground up and with Wikipedia in mind, the table of contents by
default appears before the first heading on a page. This allows the author to
insert lead-in content that may summarise or introduce the rest of the page.
It also uses a unique numbering scheme that doesn't get lost through CSS 
differences across themes.

This module is a great companion for content rich sites such as content
management system oriented configurations.

Includes an administration settings page where you can customise settings like
display position, define the minimum number of headings before an index is
displayed, other appearance, and more.  For power users, expand the advanced
options to further tweak its behaviour - eg: exclude undesired heading levels
like h5 and h6 from being included; disable the output of the included CSS file;
adjust the top offset and more.

You can select which node types should have table of contents from module
settings, so enable the ones you want.


REQUIREMENTS
------------
This module requires the following modules:
 * Libraries API 2.x (https://drupal.org/project/libraries)
 * Entity API (https://drupal.org/project/entity)
 * Transliteration (https://www.drupal.org/project/transliteration)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
 * Download the Smooth Scroll plugin. Make sure to use the 1.4.10 branch since
   that will stay backwards compatible with older jQuery version, like the ones
   included with Drupal.
   You can directly download Smooth Scroll plugin here:
   https://github.com/kswedberg/jquery-smooth-scroll/archive/1.4.10.zip


CONFIGURATION
-------------
All settings for this module are on the Super TOC configuration page, under the
Configuration section, in the Content authoring settings. You can visit the
configuration page directly at admin/config/content/super_toc.


MAINTAINERS
------
Current maintainers:
 * Mikhail Khasaya (dillix) - https://www.drupal.org/user/1676156
